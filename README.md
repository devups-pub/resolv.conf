This roles uses template for updating host's /etc/resolv.conf file.

Be aware that the commands specifically intended to query DNS (especially `host`)
do not use the standard name resolution mechanism (NSS). As a consequence, they
do not take into consideration /etc/nsswitch.conf, and thus, not /etc/hosts
either.*1

Best Free & Public DNS Servers (November 2019) *2  

Provider	Primary DNS	Secondary DNS  
Google	8.8.8.8	8.8.4.4  
Quad9	9.9.9.9	149.112.112.112  
OpenDNS Home	208.67.222.222	208.67.220.220  
Cloudflare	1.1.1.1	1.0.0.1  
CleanBrowsing	185.228.168.9	185.228.169.9  
Verisign	64.6.64.6	64.6.65.6  
Alternate DNS	198.101.242.72	23.253.163.53  
AdGuard DNS	176.103.130.130	176.103.130.131  

sources:
* code: https://www.redhat.com/sysadmin/dns-configuration-ansible
* *1 - Debian Handbook (pdf creation time 28.10.2015) page 187 of 513 (real, not page markup)
* *2 - https://www.lifewire.com/free-and-public-dns-servers-2626062